pt-BR:

O emulador de jogos PicoDrive foi criado por notaz (https://notaz.gp2x.de/pico.php). Emula os jogos do Master System, Mega Drive, Genesis, Sega CD, Mega CD, Sega 32X.

Contém: o arquivo ou pacote de instalação .deb e um arquivo de texto em idioma "pt-BR" e "fr_FR" explicando como utilizá-lo.

Transfira ou baixe ou descarregue os arquivos "picodrive_1.93-1_amd64.deb", "picodrive_1.93-1_amd64.deb.md5.sum", "picodrive_1.93-1_amd64.deb.sha256.sum" e o arquivo de texto ".txt".

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Der Spieleemulator PicoDrive wurde von notaz (https://notaz.gp2x.de/pico.php) erstellt. Emuliert Master System, Mega Drive, Genesis, Sega CD, Mega CD, Sega 32X.

Es enthält: die .deb-Installationsdatei oder das Paket und eine Textdatei in "pt-BR" und "fr_FR" -Sprache, die erklärt, wie man es benutzt.

Laden Sie die Dateien "picodrive_1.93-1_amd64.deb", "picodrive_1.93-1_amd64.deb.md5.sum", "picodrive_1.93-1_amd64.deb.sha256.sum" und die Textdatei ".txt" herunter oder laden Sie sie herunter oder laden Sie sie herunter.
Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

fr :

L'émulateur de jeu PicoDrive a été créé par notaz (https://notaz.gp2x.de/pico.php). Émule les jeux Master System, Mega Drive, Genesis, Sega CD, Mega CD, Sega 32X.

Contient : le fichier ou package d'installation .deb et un fichier texte en "pt-BR" et "fr_FR" expliquant comment l'utiliser.

Téléchargez les fichiers "picodrive_1.93-1_amd64.deb", "picodrive_1.93-1_amd64.deb.md5.sum", "picodrive_1.93-1_amd64.deb.sha256.sum" et le fichier texte ".txt".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

L'emulatore di gioco PicoDrive è stato creato da notaz (https://notaz.gp2x.de/pico.php). Emula i giochi Master System, Mega Drive, Genesis, Sega CD, Mega CD, Sega 32X.
Contiene: il file o pacchetto di installazione .deb e un file di testo in lingua "pt-BR" e "fr_FR" che spiega come utilizzarlo.

Scarica i file "picodrive_1.93-1_amd64.deb", "picodrive_1.93-1_amd64.deb.md5.sum", "picodrive_1.93-1_amd64.deb.sha256.sum" e il file di testo ".txt".

Tutti i crediti e i diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe

- - - - -

es:

El emulador de juegos PicoDrive fue creado por notaz (https://notaz.gp2x.de/pico.php). Emula juegos Master System, Mega Drive, Genesis, Sega CD, Mega CD, Sega 32X.

Contiene: el archivo o paquete de instalación .deb y un archivo de texto en idioma "pt-BR" y "fr_FR" que explica cómo usarlo.

Descargue los archivos "picodrive_1.93-1_amd64.deb", "picodrive_1.93-1_amd64.deb.md5.sum", "picodrive_1.93-1_amd64.deb.sha256.sum" y el archivo de texto ".txt".

Todos los créditos y derechos están incluidos en los archivos, en relación con el trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

en:


The PicoDrive game emulator was created by notaz (https://notaz.gp2x.de/pico.php). Emulates Master System, Mega Drive, Genesis, Sega CD, Mega CD, Sega 32X games.

Contains: the .deb installation file or package and a text file in "pt-BR" and "fr_FR" language explaining how to use it.

Download the files "picodrive_1.93-1_amd64.deb", "picodrive_1.93-1_amd64.deb.md5.sum", "picodrive_1.93-1_amd64.deb.sha256.sum" and the text file ".txt".

All credits and rights are included in the files, in respect of the volunteer work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe
